import { TestBed, inject } from '@angular/core/testing';

import { QuestionUploadService } from './question-upload.service';

describe('QuestionUploadService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [QuestionUploadService]
    });
  });

  it('should be created', inject([QuestionUploadService], (service: QuestionUploadService) => {
    expect(service).toBeTruthy();
  }));
});
