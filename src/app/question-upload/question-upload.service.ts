import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { error } from '@angular/compiler/src/util';
import { AppglobalService } from 'src/app/appglobal.service';

@Injectable({
  providedIn: 'root'
})
export class QuestionUploadService {

  constructor(private http: HttpClient,private global: AppglobalService) { }


  uploadQuestions(data: string[]){
    let httpHeaders = new HttpHeaders()
    .append('Accept', 'application/json')

   return this.http.post<any>(this.global.host+"/question/upload/questions"
            , {
              data
            }, {
                headers: httpHeaders,
                responseType: 'json'
            }
        )
  }
}
