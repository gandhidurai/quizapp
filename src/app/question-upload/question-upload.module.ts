import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuestionUploadRoutingModule } from './question-upload-routing.module';
import { QuestionUploadComponent } from './question-upload/question-upload.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    QuestionUploadRoutingModule,
    SharedModule
  ],
  declarations: [QuestionUploadComponent]
})
export class QuestionUploadModule { }
