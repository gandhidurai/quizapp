import { Component, OnInit } from '@angular/core';
import { FormGroup ,FormControl} from '@angular/forms';
import * as XLSX from 'xlsx';
import { QuestionUploadService } from 'src/app/question-upload/question-upload.service';
import { UserserviceService } from 'src/app/userservice.service';

@Component({
  selector: 'app-question-upload',
  templateUrl: './question-upload.component.html',
  styleUrls: ['./question-upload.component.css']
})
export class QuestionUploadComponent implements OnInit {
  

  totalInventories: any;
  filedata:any;
  jsonData: any[];

  validateMessage: string;
  isValid: boolean=false;
  
  fileEvent(e){
      this.filedata=e.target.files[0]; 
  }
    constructor(private questionUploadService: QuestionUploadService) { }
  
  
    ngOnInit() {
  
  
  
      
    }
  
    onSubmit() {
      if(this.jsonData!=null && this.jsonData!=undefined && this.validateMessage==''){
      this.questionUploadService.uploadQuestions(this.jsonData)
      .subscribe(data=>{
  
        console.info("mongo insert : "+JSON.stringify(data));
      },
      error=>{
        if(error.status==400){
          console.info(""+error.error.statusMessage);
        }
      }
    
    
    )
      }
   
  }
  
  clearErrorMessage(){
    this.validateMessage="";
  }
  
  
   onFileChange(evt: any) {
    /* wire up file reader */

    const target: DataTransfer = <DataTransfer>(evt.target);

    console.info(target.files[0].type);
   
    if(target.files[0].type !=='application/vnd.ms-excel' && target.files[0].type !=='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'){
      this.validateMessage="please provide xlsx or xls format file only";
      this.isValid=false;
    }else{
      this.isValid=true;
    }

    if (target.files.length !== 1) throw new Error('Cannot use multiple files');
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      /* read workbook */
      
      
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, {type: 'binary'});
  
      /* grab first sheet */
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];
  
      /* save data */
      var data = (XLSX.utils.sheet_to_json(ws, {header: 0}));
      this.jsonData=data;
    };
    
    reader.readAsBinaryString(target.files[0]);


  }
  
}
