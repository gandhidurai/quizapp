import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { QuestionUploadComponent } from 'src/app/question-upload/question-upload/question-upload.component';
import { UserGuard } from 'src/app/user.guard';
import { AdminGuard } from 'src/app/admin.guard';


const routes: Routes = [
  { path: 'questionUpload',component: QuestionUploadComponent,canActivate:[UserGuard,AdminGuard] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers:[AdminGuard]
})
export class QuestionUploadRoutingModule { }
