import { Component, OnInit } from '@angular/core';
import { Login } from '../login';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { error } from 'util';

import { UserserviceService } from 'src/app/userservice.service';

@Component({
  selector: 'app-loginform',
  templateUrl: './loginform.component.html',
  styleUrls: ['./loginform.component.css']
})
export class LoginformComponent implements OnInit {

  loginForm: FormGroup;
  validateError = "";
  loginResult: any;

  constructor(private route: Router, private userService: UserserviceService) { }

  ngOnInit() {
    this.loginForm = new FormGroup({
      'name': new FormControl('', [Validators.required]),
      'password': new FormControl('', [Validators.required])
    });
  }

  get name() { return this.loginForm.get("name"); }

  get password() { return this.loginForm.get("password"); }

  onSubmit() {
    if (this.name.value == 'admin' && this.password.value == 'admin') {
      this.route.navigate(['main']);
    }
    else {
      this.validateError = "Please provide correct credentials";
    }
/*
    this.userService.name=this.loginForm.value.name;
    this.userService.doSignin(this.name.value,this.password.value).subscribe(
      data=>{
     //console.log(data);
     this.loginResult = data;
     console.log(this.loginResult);
     if(this.loginResult.status==200 && this.loginResult.statusMessage=="Success"){
       this.route.navigate(["topic"]);
       this.userService.isLogined=true;
       this.userService.isAdmin=this.loginResult.data.admin;
       
     }
     else{
      this.validateError="Please provide correct credentials";
     }

    })*/

  }

  signUp() {
    this.route.navigate(['sign-up']);
  }
  
  admin(){
    this.route.navigate(['admin']);
  }

}
