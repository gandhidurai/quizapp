import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { UserserviceService } from '../userservice.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  signUpForm: FormGroup;
  signUpResult: any;
  validateError = "";

  constructor(private route: Router, private userService: UserserviceService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.signUpForm = this.formBuilder.group({
      firstName: new FormControl('', [Validators.required]),
      lastName: new FormControl(''),
      username: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required]),
    });
  }

  backToLogin() {
    this.route.navigate(['login']);
  }

  onSubmit(value: any) {
    //this.userService.signUp(value.firstName, value.lastName, value.email, value.username, value.password).subscribe(
      this.userService.signUp(value).subscribe(
      data => {
        this.signUpResult = data;
        console.log("signUp result========");
        console.log(this.signUpResult);
        if (this.signUpResult.status == 200 && this.signUpResult.statusMessage == "Success") {
          this.route.navigate(["topic"]);
        }
        else {
          this.validateError = "Your email is already registered with us";
        }
      })

  }
}
