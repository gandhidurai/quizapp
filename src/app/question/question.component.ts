import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { QuestionService } from '../question.service';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {

  result: any[];

  constructor(private route: Router, private questionService: QuestionService) { }

  ngOnInit() {
    console.log("inside result component");
    this.result = this.questionService.result;
    console.log(this.result);
  }

}
