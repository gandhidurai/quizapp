import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AppglobalService {

  host: string="http://localhost:8080/quizwebservice/rest";

  constructor() { }
}
