import { TestBed, inject } from '@angular/core/testing';

import { AppglobalService } from './appglobal.service';

describe('AppglobalService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AppglobalService]
    });
  });

  it('should be created', inject([AppglobalService], (service: AppglobalService) => {
    expect(service).toBeTruthy();
  }));
});
