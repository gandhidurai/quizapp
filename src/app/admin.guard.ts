import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { UserserviceService } from 'src/app/userservice.service';

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate 
{
  constructor(private userService: UserserviceService){

  }
  canActivate()
   {
    if(this.userService.isAdmin){
      return true;
    }
    else {
      window.alert("You don't have permission to view this page"); 
      return false;
    }
  }
}
