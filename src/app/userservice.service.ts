import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserserviceService {

  host: string = "http://localhost:8080/quizwebservice/rest/";
  question: any[];
  options: any[];
  answer: string;
  questionId: string;
  qnprogress: number;
  result: any[];
  name: any;
  isLogined: boolean=false;
  isAdmin: boolean=false;

  constructor(private http: HttpClient) { }

  doLogin() {
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }
    return this.http.get(this.host + "topic/getTopics", httpOptions)

  }

  domarks() {
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }
    return this.http.get(this.host + "mark/getMarks", httpOptions)
  }

  getqQuestion(topic: string, mark: string) {
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }
    console.info(this.host + "question/getQuestions/" + topic.toLowerCase() + "/" + mark);
    return this.http.get(this.host + "question/getQuestions/" + topic.toLowerCase() + "/" + mark, httpOptions)
  }

  getResults(topic: string, mark: string, answers: any) {
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }
    let body = { "topic": topic, "mark": mark, "answers": answers }
    console.info("post url " + this.host + "result/getResult");
    return this.http.post(this.host + "result/getResult", body, httpOptions)
  }

  doSignin(name: any, password: any) {
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }
    return this.http.get("http://localhost:8080/quizwebservice/rest/token/user?username=" + name + "&password=" + password, httpOptions);
  }

  signUp(body) {
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }
    console.log(body);
    return this.http.post("http://localhost:8080/quizwebservice/rest/token/createUser", body, httpOptions)
  }

  getUserDetails(name) {
    console.log("inside");
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }
    console.log("get details url--------" + "http://localhost:8080/quizwebservice/rest/token/getUserDetails?username=" + name);
    return this.http.get("http://localhost:8080/quizwebservice/rest/token/getUserDetails?username=" + name, httpOptions)
  }

}
