import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserserviceService } from 'src/app/userservice.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  isAdmin: boolean=false;
  constructor(private route: Router,private userservice: UserserviceService) { }

  ngOnInit() {
    this.isAdmin=this.userservice.isAdmin;
    console.info("improve your self " + this.isAdmin)
   }

  onSubmit() {
   this.route.navigate(['logic']);
  }

}
