import { BrowserModule } from '@angular/platform-browser';
import { NgModule, OnInit } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { LoginformComponent } from './loginform/loginform.component';
import { ReactiveFormsModule } from '@angular/forms';

import { RouterModule, Routes } from '@angular/router';
import { Login } from './login';
import { LogicComponent } from './logic/logic.component';
import { TopicComponent } from './topic/topic.component';
import { UserserviceService } from './userservice.service';
import { QuestionService } from './question.service';
import { MatButtonModule, MatRadioModule ,MatTooltipModule,MatCheckboxModule} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { QuestionComponent } from './question/question.component';
import { AdminComponent } from './admin/admin.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { QuestionUploadModule } from 'src/app/question-upload/question-upload.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { MainComponent } from 'src/app/shared/main/main.component';
import { UserdetailsComponent } from './userdetails/userdetails.component';
import { UserGuard } from 'src/app/user.guard';
import { AdminGuard } from 'src/app/admin.guard';



const appRoutes: Routes = [
  { path: '', component: LoginformComponent },
  { path: 'main', component: MainComponent,canActivate:[UserGuard] },
  { path: 'topic', component: TopicComponent,canActivate:[UserGuard] },
  { path: 'logic', component: LogicComponent,canActivate:[UserGuard] },
  { path: 'question', component: QuestionComponent,canActivate:[UserGuard] },
  { path: 'sign-up', component: SignUpComponent,canActivate:[UserGuard] },
  { path: 'admin', component: AdminComponent,canActivate:[UserGuard] },
  { path: 'userDetails', component: UserdetailsComponent,canActivate:[UserGuard] }

];

@NgModule({
  declarations: [
    AppComponent,
    LoginformComponent,
    LogicComponent,
    TopicComponent,
    QuestionComponent,
    AdminComponent,
    SignUpComponent,
    UserdetailsComponent,


  ],
  imports: [
    BrowserModule,
    SharedModule,
    QuestionUploadModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(appRoutes),
    MatRadioModule,
    MatTooltipModule,
    MatCheckboxModule
  ],
  providers: [UserserviceService,UserGuard,QuestionService],
  bootstrap: [AppComponent]

})
export class AppModule {




}
