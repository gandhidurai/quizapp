import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { Observable, range, interval } from 'rxjs';
import { take } from 'rxjs/operators';
import { timer } from 'rxjs/internal/observable/timer';

import { QuestionService } from '../question.service';
import { UserserviceService } from '../userservice.service';


@Component({
    selector: 'app-logic',
    templateUrl: './logic.component.html',
    styleUrls: ['./logic.component.css']
})
export class LogicComponent implements OnInit {

    private counter = 0;
    logicForm: FormGroup;
    question: any[];
    options: any[];
    questionId: number;
    remainElapsedSec: number = 0;
    remainElapsedMin: number = 0;
    totalTime: number;
    answers: any[] = [];
    timerUnsubcribe: any;
    currentQuestionIndex = 0;
    buttonLable: string = "Next";
    questionType: string;

    constructor(private route: Router, private userService: UserserviceService, private questionService: QuestionService) {
        this.questionService.seconds = 0;
        this.questionService.qnprogress = 0;
    }

    ngOnInit() {
        this.logicForm = new FormGroup({
            'selectedOption': new FormControl('', [Validators.required]),
            'multiple': new FormArray([])
        })
        this.timerUnsubcribe = interval(1000).subscribe(x => { });
        this.onSubmit();
    }

    get selectedOption() {
        return this.logicForm.get('selectedOption');
    }

    get multiple() {
        return this.logicForm.get('multiple');
    }

    onSubmit() {

        if (this.currentQuestionIndex != 0) {
            this.answers.splice(this.currentQuestionIndex, 1, { "questionId": this.questionId, "answer": this.selectedOption.value });
            console.info("Selected Answer : " + this.selectedOption.value);
            console.info("multiplechoice" + this.multiple.value);
        }

        if (this.buttonLable != 'End Session') {
            this.timerUnsubcribe.unsubscribe();
            this.question = this.questionService.questions[this.currentQuestionIndex].question;
            this.options = this.questionService.questions[this.currentQuestionIndex].option;
            this.questionId = this.questionService.questions[this.currentQuestionIndex].questionId;
            this.totalTime = this.questionService.questions[this.currentQuestionIndex].time;
            this.questionType = this.questionService.questions[this.currentQuestionIndex].questionType;

            this.currentQuestionIndex++;
            this.timerUnsubcribe = interval(1000).subscribe(x => {

                var min = Math.floor((((this.totalTime * 59) - x % 86400) % 3600) / 60);
                this.remainElapsedMin = min == -1 ? 0 : min;
                this.remainElapsedSec = ((this.totalTime * 60) - x) % 60;


                if (x == this.totalTime * 60) {
                    if (this.buttonLable != 'End Session') {
                        this.counter = this.currentQuestionIndex;
                        this.question = this.questionService.questions[this.currentQuestionIndex].question;
                        this.options = this.questionService.questions[this.currentQuestionIndex].option;
                        this.questionId = this.questionService.questions[this.currentQuestionIndex].questionId;
                        this.totalTime = this.questionService.questions[this.currentQuestionIndex].questionId;
                        if (this.currentQuestionIndex == this.questionService.questions.length) {
                            this.buttonLable = "End Session";
                        }
                        this.onSubmit();

                    }
                    else {
                        this.timerUnsubcribe.unsubscribe();
                        this.submitResult();
                    }
                }
            })
            if (this.currentQuestionIndex == this.questionService.questions.length) {
                this.buttonLable = 'End Session';
            }
        }
        else {
            this.timerUnsubcribe.unsubscribe();
            this.submitResult();
        }
    }

    submitResult() {
        this.userService.getResults(this.questionService.selectedTopic, this.questionService.selectedMark, this.answers).subscribe(data => {
            this.questionService.result = data;
            this.route.navigate(['question']);
        })
    }

}


