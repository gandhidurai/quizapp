import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { UserserviceService } from 'src/app/userservice.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UserGuard implements CanActivate {

  constructor(private userService: UserserviceService,private router: Router){

  }
  canActivate(){
    if(this.userService.isLogined){
      return true;
    }
    else {
      window.alert("You don't have permission to login first"); 
      this.router.navigate([""]);
      return false;
    }
  }
}
